import { ColumnOptions } from '../../types';
declare const _default: (type: string, options?: ColumnOptions) => string;
export default _default;
