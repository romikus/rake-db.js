import { ColumnFunction, ColumnOptions } from '../../types';
declare const _default: (column: ColumnFunction, options?: ColumnOptions) => void;
export default _default;
